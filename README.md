# Groundcoverify!

This is a simple python script which uses [DeltaPlugin](https://gitlab.com/bmwinger/delta-plugin) to turn regular groundcover in morrowind plugins into openmw-style groundcover.

## Usage

`./groundcoverify.py`

## Requirements

Either python 3.11 or newer, or Python 3.7 or newer with tomli installed, and [DeltaPlugin 0.19 or newer](https://gitlab.com/bmwinger/delta-plugin).

Note that there is a bug with deleting cellrefs in OpenMW 0.47 which prevents this from working properly. It has been fixed in newer versions (at the time of writing, 0.48 is not yet stable, but it works in the release candidates). 
